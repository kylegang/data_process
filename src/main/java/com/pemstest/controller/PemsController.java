package com.pemstest.controller;

import cn.afterturn.easypoi.entity.vo.TemplateExcelConstants;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import cn.afterturn.easypoi.view.PoiBaseView;
import com.pemstest.domain.pems.PemsReportEntity;
import com.pemstest.service.pems.EmissionDataService;
import com.pemstest.utils.Mutil;
import com.pemstest.utils.Result;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author KYLE
 */

@Controller
@RequestMapping("/")
@ResponseBody
@Scope("session")
public class PemsController {

    @Autowired
    private EmissionDataService emissionDataService;

    /**
     * 是否增加@Scope("session") ，设置会话级别的。下载后是否删除对象值
     */
    private Object reportEntity = new PemsReportEntity();

    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("index");
        return mav;
    }

    @RequestMapping("/pems")
    public ModelAndView pems() {
        ModelAndView mav = new ModelAndView("pemsReport");

        return mav;
    }


    /**
     * CSV数据导入
     *
     * @return
     */

    @RequestMapping(value = "/pems/import", method = RequestMethod.POST)
    @ResponseBody
    public Result upLoad(@RequestParam(value = "files", required = false) MultipartFile file,
                         HttpServletRequest request) {
        Result msg = new Result();
        msg.setSuccess(false);

        if (!file.isEmpty()) {
            List list = emissionDataService.getData(file,request);
            reportEntity = emissionDataService.proceeData(list,request);

            msg = Result.ok();
        }
        return msg;
    }


    @RequestMapping("/pems/download")
    @ResponseBody
    public void downLoad(ModelMap map, HttpServletRequest request, HttpServletResponse response){
        try
        {


            File templeName= ResourceUtils.getFile("classpath:template/PEMS.xls");
            Map<String, Object> obj = new HashMap<>(PemsController.beanToMap(reportEntity));

            TemplateExportParams params = new TemplateExportParams(templeName.getPath());
            Workbook book = ExcelExportUtil.exportExcel(params,obj);
            String fileName = templeName.getName();

            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);

            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));

            book.write(response.getOutputStream());

        }catch (IOException e){
            e.printStackTrace();
        }
    }


    @RequestMapping("/pems/excelload")
    public void down2load(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response){

        TemplateExportParams params = new TemplateExportParams(
                "template/PEMS.xlsx");
        Map<String, Object> map = new HashMap<>(PemsController.beanToMap(reportEntity));

        SimpleDateFormat simdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        modelMap.put(TemplateExcelConstants.FILE_NAME, "PEMS" + simdate.format(new Date()));
        modelMap.put(TemplateExcelConstants.PARAMS, params);
        modelMap.put(TemplateExcelConstants.MAP_DATA, map);
        PoiBaseView.render(modelMap, request, response,
                TemplateExcelConstants.EASYPOI_TEMPLATE_EXCEL_VIEW);

        reportEntity = null;
    }

    @RequestMapping("/pems/wordload")
    @ResponseBody
    public void wordLoad(ModelMap map, HttpServletRequest request, HttpServletResponse response){
        try
        {


            File templeName= ResourceUtils.getFile("classpath:template/PEMS结果页.doc");
            Map<String, Object> obj = new HashMap<>(PemsController.beanToMap(reportEntity));

            TemplateExportParams params = new TemplateExportParams(templeName.getPath());
            Workbook book = ExcelExportUtil.exportExcel(params,obj);
            String fileName = templeName.getName();

            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);

            /*
            response.setContentType("application/octet-stream");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            //response.setHeader("content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
             */

            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));

            book.write(response.getOutputStream());

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static <T> Map<String, Object> beanToMap(T bean) {
        Map<String, Object> map = new HashMap();
        if (bean != null) {

            Class clazz = bean.getClass();
            Field[] fields = clazz.getDeclaredFields();
            try {
                for (Field field : fields) {
                    field.setAccessible(true);

                    if (field.getGenericType().toString().equals("double")){
                        double t = Double.valueOf(field.get(bean).toString());
                        //t = Mutil.round(t,4);
                        map.put(field.getName(), Mutil.round(t,4));
                    }else {
                        map.put(field.getName(), field.get(bean));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }


}
