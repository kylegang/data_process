package com.pemstest.service.pems.impl;

import com.pemstest.domain.pems.PemsReportEntity;
import com.pemstest.domain.pems.PemsTestEntity;
import com.pemstest.service.pems.EmissionDataService;
import com.pemstest.utils.DataProcess;
import com.pemstest.utils.ReadCSV;
import com.pemstest.utils.Result;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author KYLE
 */
@Service
public class PemsServiceImpl implements EmissionDataService<PemsTestEntity, PemsReportEntity> {

    private Result result1 = new Result();

    @Override
    public List<PemsTestEntity> getData(MultipartFile file, HttpServletRequest request) {

        try {
            InputStreamReader isr = new InputStreamReader(file.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            List<Map<String,String>> retList = ReadCSV.getcsvMultipartFile(file,3);
            List<PemsTestEntity> datalist = new ArrayList<>();

            PemsTestEntity pemsTestEntity;
            for (Map<String,String> m:retList){
                pemsTestEntity = DataProcess.mapToEntity(m,PemsTestEntity.class);
                datalist.add(pemsTestEntity);
            }

            List<PemsTestEntity> realdatalist = null;
            int realdata_num = 0;
            try {
                DataProcess ps = new DataProcess();
                realdatalist = ps.getRealData(datalist);
                result1 = ps.result;
            }catch (Exception e){
                e.printStackTrace();
            }
            return realdatalist;

        }catch (Exception e){
            e.printStackTrace();
        }


        return null;
    }


    @Override
    public PemsReportEntity proceeData(List<PemsTestEntity> list, HttpServletRequest request) {
        //PEMS独有变量
        double circulationPower = Double.valueOf(request.getParameter("circulationPower"));
        double ratedPower = Double.valueOf(request.getParameter("ratedPower"));
        //double circulationPower = 118;
        //double ratedPower = 10.01;

        PemsReportEntity pemsReport = new PemsReportEntity();
        //
        pemsReport.setCirculationPower(circulationPower);

        int time = list.size();
        pemsReport.setExpNum(time);
        pemsReport.setExpHours(Double.valueOf(time)/3600);

        DoubleSummaryStatistics gps_vs = list.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getGPS_VehicleSpeed));
        double mileage_sum = gps_vs.getSum()/3600;
        pemsReport.setMileageSum(mileage_sum);
        //大气温度
        DoubleSummaryStatistics ws_at = list.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getWS_AmbientTemp));
        pemsReport.setWsat_min(ws_at.getMin());
        pemsReport.setWsat_max(ws_at.getMax());
        pemsReport.setWsat_avg(ws_at.getAverage());
        //大气湿度
        DoubleSummaryStatistics ws_arh = list.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getWS_AmbientRelativeHumidity));
        pemsReport.setWs_arh_min(ws_arh.getMin());
        pemsReport.setWs_arh_max(ws_arh.getMax());
        pemsReport.setWs_arh_avg(ws_arh.getAverage());
        //大气压力
        DoubleSummaryStatistics ws_ap = list.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getWS_AmbientPressure));
        pemsReport.setWs_ap_min(ws_ap.getMin());
        pemsReport.setWs_ap_max(ws_ap.getMax());
        pemsReport.setWs_ap_avg(ws_ap.getAverage());
        //海拔
        DoubleSummaryStatistics gps_a = list.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getGPS_Altitude));
        pemsReport.setGps_a_min(gps_a.getMin());
        pemsReport.setGps_a_max(gps_a.getMax());
        pemsReport.setGps_a_avg(gps_a.getAverage());

        pemsReport.setGps_a_start(list.get(0).getGPS_Altitude());
        pemsReport.setGps_a_end(list.get(time -1).getGPS_Altitude());
        pemsReport.setGps_a_abs(Math.abs(pemsReport.getGps_a_end() - pemsReport.getGps_a_start()));


        List<Double> old_gps_a = list.stream().map(PemsTestEntity::getGPS_Altitude).collect(Collectors.toList());
        List<Double> new_gps_a = new ArrayList<>();
        new_gps_a.add(0.0);
        for (int i = 1; i < time; i++){
            new_gps_a.add(Math.max(0,(old_gps_a.get(i)-old_gps_a.get(i-1))));
        }
        double sum_gps_a = new_gps_a.stream().collect(Collectors.summingDouble(s -> s));
        //累计海拔增量
        pemsReport.setGps_a_mileage_sum(100 * sum_gps_a/mileage_sum);

        List<Double> old_gps_vs =list.stream().map(PemsTestEntity::getGPS_VehicleSpeed).collect(Collectors.toList());
        List<Double> new_gps_vs = new ArrayList<>();
        new_gps_vs.add(0.0);
        for (int i = 1;i < time -1;i++){
            new_gps_vs.add((old_gps_vs.get(i+1)-old_gps_vs.get(i-1))/7.2);
        }
        new_gps_vs.add((old_gps_vs.get(time -1)-old_gps_vs.get(time-2))/3.6);

        double idle_num = old_gps_vs.stream().filter(s ->s < 1).count();
        double upspeed_num = new_gps_vs.stream().filter(s -> s >= 0.1).count();
        double downspeed_num = new_gps_vs.stream().filter(s -> s <= -0.1).count();

        pemsReport.setUpSpeedRatio(upspeed_num/time*100);
        pemsReport.setDownSpeedRatio(downspeed_num/time*100);
        pemsReport.setIdleSpeedRatio(idle_num/time*100);
        pemsReport.setConstantSpeedRatio((time - upspeed_num - downspeed_num -idle_num)/time*100);


        int suburb_start = 0;
        int suburb_tar = 0;
        int highe_tar = 0;
        for (int i = 1;i < time -1;i++){
            if (old_gps_vs.get(i) >= 55 && old_gps_vs.get(i) < (old_gps_vs.get(i-1) + 5) && old_gps_vs.get(i) > (old_gps_vs.get(i+1) - 5)){
                suburb_tar = i;
                break;
            }
        }
        for (int i = suburb_tar;i >= 0;i--){
            if (old_gps_vs.get(i) < 1 ){
                suburb_start =i;
                break;
            }
        }
        int high_start = 0;
        for (int i = suburb_tar;i < time;i++){
            if (old_gps_vs.get(i) > 75 && old_gps_vs.get(i) < (old_gps_vs.get(i-1) + 5) && old_gps_vs.get(i) > (old_gps_vs.get(i+1) - 5)){
                highe_tar =i;
                break;
            }
        }
        for  (int i = highe_tar;i > suburb_tar;i--){
            if (old_gps_vs.get(i) < 1){
                high_start =i;
                break;
            }
        }

        //System.out.println("市郊开始车速" + list.get(suburb_start).getGPS_VehicleSpeed()  + "高速开始车速" + list.get(high_start).getGPS_VehicleSpeed() );

        double city_speed = old_gps_vs.subList(0, suburb_start -1).stream().collect(Collectors.averagingDouble(s -> s));
        double suburb_speed = 0;
        double high_speed = 0;
        if(high_start == 0){
            suburb_speed = old_gps_vs.subList(suburb_start, time).stream().collect(Collectors.averagingDouble(s -> s));
        }else {
            suburb_speed = old_gps_vs.subList(suburb_start, high_start -1).stream().collect(Collectors.averagingDouble(s -> s));
            high_speed = old_gps_vs.subList(high_start, time).stream().collect(Collectors.averagingDouble(s -> s));
        }

        pemsReport.setSuburbSpeed(suburb_speed);
        pemsReport.setCitySpeed(city_speed);
        pemsReport.setHighSpeed(high_speed);
        pemsReport.setCityRatio((double) (suburb_start) / (double) time * 100);
        if(high_speed == 0){
            pemsReport.setSuburbRatio((double) (time - suburb_start + 1) / (double) time * 100);
        }else {
            pemsReport.setSuburbRatio((double) (high_start - suburb_start) / (double) time * 100);
            pemsReport.setHighRatio((double) (time - high_start + 1) / (double) time * 100);
        }

        //一致性相关系数
        List fcb1_list = list.stream().map(PemsTestEntity::getFuel_CB_1).collect(Collectors.toList());
        List obd_efr_list = null;
        if (!(list.get(0).getOBD_EngineFuelRate() == null)){
            obd_efr_list = list.stream().map(PemsTestEntity::getOBD_EngineFuelRate).collect(Collectors.toList());

        }else if (!(list.get(0).getOBDFuelRate() == null)){
            obd_efr_list = list.stream().map(PemsTestEntity::getOBDFuelRate).collect(Collectors.toList());
        }
        pemsReport.setR2(DataProcess.correlation(obd_efr_list,fcb1_list));
        pemsReport.setRemark(result1.getMsg());
        
        //扭矩计算
        List<Double> powerlist = list.stream()
                .map(s -> (s.getOBD_ActualEnginePercentTorque() - s.getOBD_NominalFrictionPercentTorque())* s.getOBD_EngineReferenceTorque()/100).collect(Collectors.toList());
        //总功
        Double g_sum = list.stream().map(s -> Math.max(0, (s.getOBD_ActualEnginePercentTorque() - s.getOBD_NominalFrictionPercentTorque())) * s.getOBD_EngineReferenceTorque() * s.getOBD_EngineSpeed() / 100 / 9550 / 3600).mapToDouble(t -> t).sum();
        //实验总功
        pemsReport.setSumPower(g_sum);


        //计算功率
        list
                .stream()
                .map(s ->{
                    Double ll = Math.max(0,(s.getOBD_ActualEnginePercentTorque() - s.getOBD_NominalFrictionPercentTorque()))* s.getOBD_EngineReferenceTorque() *s.getOBD_EngineSpeed()/100/9550/3600;
                    s.setOBD_Power(ll);
                    return s;
                }).collect(Collectors.toList());


        //生成新的集合，OBD_Power1保存平均值，其余保存求和值
        List<PemsTestEntity> list_win = new ArrayList<>();
        for(int i = 0; i < time; i++){
            PemsTestEntity pte_power = new PemsTestEntity();
            boolean flag = false;

            Double obd_power = 0.0;
            double co = 0.0;
            double co2 = 0.0;
            double nox = 0.0;
            double pn = 0.0;
            double thc = 0.0;

            if(list.get(0).getTHC_MASS_1() == null){
                for (int j = i; j < time; j++){
                    obd_power = obd_power + list.get(j).getOBD_Power();
                    co = co + list.get(j).getCO_MASS_1();
                    co2 = co2 + list.get(j).getCO2_MASS_1();
                    nox = nox + list.get(j).getNOx_MASS_1();
                    pn = pn + list.get(j).getPN_Inst_1();

                    Double obd_power1 = obd_power/(j-i);

                    if(obd_power >= circulationPower){

                        try {
                            pte_power.setOBD_Power(obd_power);
                            pte_power.setCO_MASS_1(co);
                            pte_power.setCO2_MASS_1(co2);
                            pte_power.setNOx_MASS_1(nox);

                            pte_power.setPN_Inst_1(pn);

                            pte_power.setOBD_Power1(obd_power1);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    }
                    if(j == time - 1 & obd_power < circulationPower){
                        flag = true;
                    }
                }
            }else {
                for (int j = i; j < time; j++){
                    obd_power = obd_power + list.get(j).getOBD_Power();
                    co = co + list.get(j).getCO_MASS_1();
                    co2 = co2 + list.get(j).getCO2_MASS_1();
                    nox = nox + list.get(j).getNOx_MASS_1();
                    thc =thc + list.get(j).getTHC_MASS_1();

                    Double obd_power1 = obd_power/(j-i);
                    if(obd_power >= circulationPower){

                        try {
                            pte_power.setOBD_Power(obd_power);
                            pte_power.setCO_MASS_1(co);
                            pte_power.setCO2_MASS_1(co2);
                            pte_power.setNOx_MASS_1(nox);

                            pte_power.setTHC_MASS_1(thc);

                            pte_power.setOBD_Power1(obd_power1);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    }
                    if(j == time - 1 & obd_power < circulationPower){
                        flag = true;
                    }
                }
            }


            if(flag){
                break;
            }
            list_win.add(pte_power);

        }
        //窗口数
        int windowNum = list_win.size();
        pemsReport.setWindowNum(windowNum);


        Integer over_num = 0;
        //有效窗口数
        List<PemsTestEntity> realwind_list = new ArrayList<>();

        for (int i = 20;i > 0;i--){
            int finalI = i;
            realwind_list = list_win.stream()
                    .filter(s -> s.getOBD_Power1()*3600/ratedPower*100 > finalI)
                    .collect(Collectors.toList());
            if (realwind_list.size() > list_win.size()*0.5){
                over_num = i;
                break;
            }
        }
        //有效窗口数、有效窗口的功率阈值
        int windowRealNum = realwind_list.size();
        pemsReport.setWindowRealNum(windowRealNum);
        pemsReport.setWindowVaule(over_num);
        pemsReport.setWindowRealRatio((double) windowRealNum/(double)windowNum*100);

        //功率阈值20的有效窗口数、占比
        long window20Num = list_win.stream()
                .filter(s -> s.getOBD_Power1()*3600/ratedPower*100 > 20)
                .count();
        pemsReport.setWindow20Num((int) window20Num);
        pemsReport.setWindow20Ratio((double) window20Num/(double)windowNum*100);

        //排放处理，将负值修正为零而不是去掉负值。
        DoubleSummaryStatistics dnox =list.stream().map(s -> Math.max(0,s.getGA_NOxConc()))
                .collect(Collectors.summarizingDouble(s -> s));

        //NOX浓度
        pemsReport.setNoConcentrationMin(dnox.getMin());
        pemsReport.setNoConcentrationMax(dnox.getMax());
        pemsReport.setNoConcentrationAvg(dnox.getAverage());

        long morefh = list.stream().filter(pemsTestEntity1 -> pemsTestEntity1.getGA_NOxConc() < 500).count();
        //浓度小于500的窗口数
        pemsReport.setNoRealConcentrationNum((int) morefh);
        //浓度小于500的比例
        pemsReport.setNoRealConcentrationRatio((double) morefh /time *100);

        //从小到大排序后最接近95%的浓度值。
        double tar_nox = list.stream().map(PemsTestEntity::getGA_NOxConc).sorted().skip(Math.round(0.95*time)-1).limit(1).collect(Collectors.toList()).get(0);
        //排序接近95%的浓度
        pemsReport.setNo95Concentration(tar_nox);

        //NOX排放总量,单位g变mg
        double nox_sum = list.stream().map(s -> Math.max(0, s.getNOx_MASS_1())).mapToDouble(s -> s).sum() *1000;
        pemsReport.setNoSum(nox_sum);
        pemsReport.setNoAvg(nox_sum/mileage_sum);
        pemsReport.setNoRatioAvg(nox_sum/g_sum);

        //NOX合格窗口数
        float nox_num =realwind_list.stream().filter(s -> s.getNOx_MASS_1() < s.getOBD_Power() * 0.69).count();
        pemsReport.setNoRatio((double)nox_num/windowRealNum*100);

        //CO排放总量,单位g变mg
        double co_sum = list.stream().map(s -> Math.max(0, s.getCO_MASS_1())).mapToDouble(s -> s).sum() *1000;
        pemsReport.setCoSum(co_sum);
        pemsReport.setCoAvg(co_sum/mileage_sum);
        pemsReport.setCoRatioAvg( co_sum/g_sum);

        //CO合格窗口数
        float co_num =realwind_list.stream().filter(s -> s.getCO_MASS_1() < s.getOBD_Power() * 6).count();
        pemsReport.setCoRatio((double)co_num/windowRealNum*100);

        //CO2排放总量,单位g变mg
        double co2_sum = list.stream().map(s -> Math.max(0, s.getCO2_MASS_1())).mapToDouble(s -> s).sum() *1000;
        pemsReport.setCo2Sum(co2_sum);
        pemsReport.setCo2Avg(co2_sum/mileage_sum);
        pemsReport.setCo2RatioAvg(co2_sum/g_sum);

        //PN和THC，没有为零
            //PN排放总量
        if (!(list.get(0).getPN_Inst_1() == null)){
            double pn_sum = list.stream().map(s -> Math.max(0, s.getPN_Inst_1())).mapToDouble(s -> s).sum();
            pemsReport.setPnSum(pn_sum);
            pemsReport.setPnAvg(pn_sum/mileage_sum);
            pemsReport.setPnRatioAvg(pn_sum/g_sum);

            //PN合格窗口数
            float pn_num =realwind_list.stream().filter(s -> s.getPN_Inst_1() < s.getOBD_Power() * 1.2E12).count();
            //PN窗口通过率,有效窗口数
            pemsReport.setPnRatio((double)pn_num/windowRealNum*100);
        }

            //THC排放总量
        if (!(list.get(0).getTHC_MASS_1() == null)) {

            double thc_sum = list.stream().map(s -> Math.max(0, s.getTHC_MASS_1())).mapToDouble(s -> s).sum()*1000;
            pemsReport.setThcSum(thc_sum);
            pemsReport.setThcAvg(thc_sum/mileage_sum);
            pemsReport.setThcRatioAvg(thc_sum/g_sum);

            //THC合格窗口数
            float thc_num =realwind_list.stream().filter(s -> s.getTHC_MASS_1() < s.getOBD_Power() * 0.75).count();
            //THC窗口通过率,有效窗口数
            pemsReport.setThcRatio((double)thc_num/windowRealNum*100);
        }

        return pemsReport;
    }
    /**
     public void printRepor(PemsReportEntity pemsReport){

     try {
     File templeName = null;
     File downloadTemplate= ResourceUtils.getFile("classpath:template/sample/PEMS结果页.xlsx");

     XWPFTemplate template = XWPFTemplate.compile(templeName.getPath()).render(pemsReport);
     String fileName = downloadTemplate.getName();

     response.setContentType("application/octet-stream");
     response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
     template.write(response.getOutputStream());

     }catch (FileNotFoundException e){
     e.printStackTrace();
     }catch (IOException e){
     e.printStackTrace();
     }

     }
     */
}
