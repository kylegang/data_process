package com.pemstest.service.pems;


import com.pemstest.domain.FuelEnum;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author KYLE
 */
public interface EmissionDataService<T,R> {

    /**
     * 导入数据
     * @param file
     * @param importTemplatePath
     * @param originalName
     * @param t
     * @return
     */
    //Boolean pemsImport(File file, String importTemplatePath, String originalName, PemsTestEntity t);
    /**
     *读取数据
     * @param file
     * @return
     */
    List<T> getData(MultipartFile file, HttpServletRequest request);
    /**
     * 处理数据
     * @param list
     * @return
     */
    R proceeData(List<T> list, HttpServletRequest request);
}
