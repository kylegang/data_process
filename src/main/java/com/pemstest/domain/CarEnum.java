package com.pemstest.domain;

import java.util.HashMap;
import java.util.Map;

public enum CarEnum {
    m1car("1", "M1车型"),
    n1car("2", "N1车型"),
    othercar("3", "其他车型");

    private String  code;
    private String desc;
    private static Map<String , CarEnum> cache = new HashMap<>();

    private static Map<String, String> sourceMap = new HashMap<>();
    private static Map<String, String> codeMap = new HashMap<>();

    CarEnum(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    static {
        CarEnum[] types = CarEnum.values();
        for (CarEnum type : types) {
            cache.put(type.code, type);
        }
        for (CarEnum carEnum : CarEnum.values()) {
            codeMap.put(carEnum.desc, carEnum.code);
            sourceMap.put(carEnum.code, carEnum.desc);
        }
    }

    public String code() {
        return code;
    }

    public String desc() {
        return desc;
    }

    public static String getCode(String source) {
        return codeMap.get(source);
    }

    public static String getSource(String code) {
        return sourceMap.get(code);
    }
}
