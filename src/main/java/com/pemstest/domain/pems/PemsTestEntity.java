package com.pemstest.domain.pems;

import lombok.Data;

import java.io.Serializable;

@Data
public class PemsTestEntity implements Serializable {

    private String Absolute_time;
    private Integer Relative_time;
    private Integer OBD_EngineCoolantTemp;  //发动机温度
    private Double OBD_EngineSpeed;       //发动机转速，单位："rpm"
    private Double GPS_VehicleSpeed;  //速度，单位:KM/h

    private Double OBD_ActualEnginePercentTorque;  //扭矩值
    private Double OBD_EngineReferenceTorque;  //扭矩
    private Double OBD_NominalFrictionPercentTorque;  //扭矩误差
    private Double OBD_Power;   //功率计算字段
    private Double OBD_Power1;   //平均功率

    private Double CO_MASS_1;  //CO，一氧化碳
    private Double CO2_MASS_1;  //CO2，二氧化碳
    private Double NOx_MASS_1;  //NO   氮氧化物
    private Double PN_Inst_1;  //磷化物
    private Double THC_MASS_1;   //碳氢物

    private Double Fuel_CB_1;  //计算燃油消耗
    private Double OBD_EngineFuelRate;  //测量燃油消耗GA_NOxConc
    private Double OBDFuelRate;

    private Double GA_NOxConc;  //NOX浓度
    private Double GPS_Altitude;  //海拔
    private Double WS_AmbientTemp;  //温度
    private Double WS_AmbientRelativeHumidity;  //湿度
    private Double WS_AmbientPressure;  //大气压强


    /*
    private Integer SYS_TestSequenceNumber;
    private Boolean SYS_TestStatus;
    private Integer SYS_TestRunStatus;
    private Boolean SYS_TestResultStatus;
    private Double SYS_TestTime;
    private Double SYS_TestDistance;
    private Integer SYS_PhaseNo;
    private Integer SYS_PhaseTime;
    private Integer SYS_TotalPhaseTime;
    private Integer GPS_GPSStatus;

    private Double GPS_Latitude;
    private Double GPS_Longitude;
    private Double GPS_VehicleSpeed;
    private Double GPS_Course;
    private Integer GPS_GPSSatelliteId;
    private Integer GPS_GPSNumberOfSatellite;
    private Double BT_BatteryVoltage;


    private Double PF_ExhaustFlowRate;
    private Double PF_ExhaustFlowRate1;
    private Double PF_ExhaustTemp1;
    private Double PF_ExhaustPressure1;
    private Double PF_ExhaustFlowRate2;
    private Double PF_ExhaustTemp2;
    private Double PF_ExhaustPressure2;
    private Double PF_ExhaustDiffPressure1;
    private Double PF_ExhaustDiffPressure2;
    private Integer GA_Operation;
    private Integer GA_Sequence;
    private Double GA_COConc;
    private Double GA_CO2Conc;
    private Double GA_H2OConc;
    private Double GA_NOConc;

    private Double GA_NO2Conc;
    private Double PN_Concentration;
    private Double PN_TotalDF;
    private Double PN_1stDF;
    private Double PN_2ndDF;
    private Double PN_CPC_Temp;
    private Double PN_CPC_Pressure;

    private Integer OBD_VehicleSpeed;
    private Integer OBD_IntakeAirTemp;
    private Integer OBD_MassAirFlowRate;
    private Integer OBD_CommandedEGR;
    private Integer OBD_BarometricPressure;
    private Integer OBD_AmbientAirTemp;
    private Integer OBD_EngineOilTemp;


    private Boolean OBD_DPF_Regeneration_Status;
    private Double TailpipeAnalyzerTemp;
    private Double TailpipeAmbientTemp;
    private Double TailpipeAmbientRelativeHumidity;
    private Double TailpipeAmbientAbsoluteHumidity;
    private Double OBDCalculatedLOADValue;

    private Double OBD_VehicleSpeed_Max;
    private Double PN_Concentration_0degC;
    private Double Absolute_Humidity;
    private Double ExhaustFlow_MASS_1;

    private Double NOxhumuncorre_MASS_1;
    private Double NOxhumcorre_MASS_1;
    private Double NO_MASS_1;
    private Double NOx_Molecular_MASS_1;
    private Double NO2_MASS_1;

    private Double Fuel_OBD_1;

    private Double Fuel_CFR_1;
    private Double FuelEconomy_1;
    private Double FrictionTorque_1;
    private Double FrictionTorque_Zero;
    private Double Work_1;
    private Double Work_Zero;
    private Double Distance_1;
    private Double GA_NOxConc_Band_1_HumCor_1;
    private Integer Loss_Time;
    private Double Torque_Zero;
    private Double GA_COConc_Band_1;
    private Double GA_CO2Conc_Band_1;
    private Double GA_NOConc_Band_1;
    private Double GA_NOxConc_Band_1;
    private Double GA_NO2Conc_Band_1;

     */

}
