package com.pemstest.domain.pems;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class PemsReportEntity implements Serializable {

    private String fuelEnum;
    private String carEnum;
    private String name;
    private String model;
    private String vin;
    private Date expTime;
    private double  designQuality;
    private double expQuality;
    //实验载荷
    private double expLoad;

    //里程表读数（km）;
    private double mileage;

    //燃油生产商
    private String fuelPrcduce;
    private String fuelModel;
    //润滑油生产商
    private String lubricatingOilPrcduce;
    private String lubricatingOilModel;
    //反应剂生产商
    private String reactantPrcduce;
    private String reactantModel;
    //甲烷
    private String methane;
    //乙烷
    private String ethane;
    //氨气
    private String ammoniaGas;
    //硫
    private String  sulphur;

    //循环功
    private double circulationPower;
    //额定功率
    private double ratedPower;
    //累计功/WHTC循环功
    private double sumDivisionCirculation;


    //实验时长
    private int expNum;
    private double expHours;
    //行驶总里程
    private double mileageSum;
    //实验总功
    private double sumPower;
    //大气温度
    private double wsat_min;
    private double wsat_max;
    private double wsat_avg;

    //大气湿度
    private double ws_arh_min;
    private double ws_arh_max;
    private double ws_arh_avg;

    //大气压力最小值
    private double ws_ap_min;
    private double ws_ap_max;
    private double ws_ap_avg;

    //海拔
    private double gps_a_min;
    private double gps_a_max;
    private double gps_a_avg;
    private double gps_a_start;
    private double gps_a_end;
    //海拔落差;
    private double gps_a_abs;
    //累计海拔增量
    private double gps_a_mileage_sum;


    //'道路工况分配比例;
    private double upSpeedRatio;
    private double downSpeedRatio;
    //怠速;
    private double idleSpeedRatio;
    //匀速;
    private double constantSpeedRatio;

    //各路段速度及组成比例;
    private double suburbSpeed;
    private double citySpeed;
    private double highSpeed;
    private double suburbRatio;
    private double cityRatio;
    private double highRatio;

    //燃油消耗量相关系数r2;
    private double r2;
    private String remark;

    //窗口数
    private int windowNum;
    //有效窗口阀值、数量
    private int windowVaule;
    private int windowRealNum;
    private double windowRealRatio;
    //阀值为20的有效串口占比
    private int window20Num;
    private double window20Ratio;

    private double coSum;
    private double coAvg;
    private double coRatioAvg;
    private double coRatio;

    private double thcSum;
    private double thcAvg;
    private double thcRatioAvg;
    private double thcRatio;

    private double pnSum;
    private double pnAvg;
    private double pnRatioAvg;
    private double pnRatio;

    private double co2Sum;
    private double co2Avg;
    private double co2RatioAvg;

    private double noSum;
    private double noAvg;
    private double noRatioAvg;
    private double noRatio;

    private double noConcentrationMin;
    private double noConcentrationMax;
    private double noConcentrationAvg;
    private double no95Concentration;
    private int noRealConcentrationNum;
    private double noRealConcentrationRatio;


}
