package com.pemstest.domain;

import java.util.HashMap;
import java.util.Map;

public enum FuelEnum {
    diesel("1", "柴油"),
    png("2", "PNG");

    private String  code;
    private String desc;
    private static Map<String , FuelEnum> cache = new HashMap<>();

    private static Map<String, String> sourceMap = new HashMap<>();
    private static Map<String, String> codeMap = new HashMap<>();

    FuelEnum(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    static {
        FuelEnum[] types = FuelEnum.values();
        for (FuelEnum type : types) {
            cache.put(type.code, type);
        }
        for (FuelEnum fuelEnum : FuelEnum.values()) {
            codeMap.put(fuelEnum.desc, fuelEnum.code);
            sourceMap.put(fuelEnum.code, fuelEnum.desc);
        }
    }

    public String code() {
        return code;
    }

    public String desc() {
        return desc;
    }

    public static String getCode(String source) {
        return codeMap.get(source);
    }

    public static String getSource(String code) {
        return sourceMap.get(code);
    }
}
