package com.pemstest.utils;

import com.pemstest.domain.pems.PemsTestEntity;

import java.io.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


public class DataProcess {

    public Result result = new Result();
    /**
     * @描述:寻找有效数据段。温度达到70后的第一个怠速终点开始，到发动机转速为0（以后未启动）结束。
     * @param listentity 原始数据实体的集合
     * @return
     */
    public List<PemsTestEntity> getRealData(List<PemsTestEntity> listentity) throws Exception {
        //第一个冷却液大于等于70的怠速终点
        Integer t = listentity.stream()
                .filter(s -> s.getOBD_EngineCoolantTemp() >= 70)
                .findFirst()
                .map(s ->s.getRelative_time()).orElse(null);

        Integer t1 = listentity.stream()
                .skip(t)
                .filter(s -> s.getGPS_VehicleSpeed() < 1)
                .findFirst()
                .map(s ->s.getRelative_time()).orElse(null);

        Integer startNum = listentity.stream()
                .skip(t1)
                .filter(s -> s.getGPS_VehicleSpeed() >= 1)
                .findFirst()
                .map(s ->s.getRelative_time()).orElse(null);

        //发送机转速降为0的最后一个数据
        List<PemsTestEntity> relist = listentity;
        Collections.reverse(relist);
        Integer overNum = relist.stream()
                .filter(s -> s.getOBD_EngineSpeed() > 0)
                .findFirst()
                .map(s ->s.getRelative_time()).orElse(null);
        //前后多留一行，以便错位处理时取数
        Collections.reverse(listentity);
        List<PemsTestEntity> list = listentity.stream()
                .filter(s -> s.getRelative_time() >= startNum - 2)
                .filter(s -> s.getRelative_time() <= overNum + 1)
                .collect(Collectors.toList());


        List<PemsTestEntity> list1 = list.subList(1,list.size() - 1);
        if (getR2(list1) >= 0.9){
            return list1;
        }else {
            List<PemsTestEntity> fix_list = new ArrayList<>();
            fix_list = fixList(deepCopy(list),true);
            result.setMsg("分析仪数据上移");
            if (getR2(fix_list) >= 0.9){
                return fix_list;
            }else {
                fix_list = fixList(deepCopy(list),false);
                result.setMsg("分析仪数据下移");
                if(getR2(fix_list) >= 0.9){
                    return fix_list;
                }else {
                    result.setMsg("归零修正");
                    return toZero(list1);
                }
            }
        }
    }

    /**
     * 错位处理数据
     * @param list
     * @param OBD
     * @return
     */
    public static List<PemsTestEntity> fixList(List<PemsTestEntity> list,Boolean OBD) throws IOException, ClassNotFoundException {
        List<PemsTestEntity> fixedList = list.subList(1,list.size()-1);
        //将OBD_EngineFuelRate相关数据上移一行
        List<PemsTestEntity> newList = new ArrayList<>();
        if(OBD){
            List<PemsTestEntity> tempList = new ArrayList<>(list.subList(0,list.size()-2));
            newList = deepCopy(tempList);
        }else {
            List<PemsTestEntity> tempList = new ArrayList<>(list.subList(2,list.size()));
            newList = deepCopy(tempList);
        }
        for (int i=0;i<fixedList.size();i++){

            fixedList.get(i).setOBD_ActualEnginePercentTorque(newList.get(i).getOBD_ActualEnginePercentTorque());
            fixedList.get(i).setOBD_EngineCoolantTemp(newList.get(i).getOBD_EngineCoolantTemp());
            fixedList.get(i).setOBD_EngineFuelRate(newList.get(i).getOBD_EngineFuelRate());
            fixedList.get(i).setOBD_NominalFrictionPercentTorque(newList.get(i).getOBD_NominalFrictionPercentTorque());
            fixedList.get(i).setOBD_EngineReferenceTorque(newList.get(i).getOBD_EngineReferenceTorque());
            fixedList.get(i).setOBD_EngineSpeed(newList.get(i).getOBD_EngineSpeed());
            fixedList.get(i).setOBDFuelRate(newList.get(i).getOBDFuelRate());
        }
        return fixedList;
    }

    /**
     * 深度复制
     * @param src
     * @param <T>
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static <T> List<T> deepCopy(List<T> src) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(src);

        ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
        ObjectInputStream in = new ObjectInputStream(byteIn);
        @SuppressWarnings("unchecked")
        List<T> dest = (List<T>) in.readObject();
        return dest;
    }

    /**
     * 小于最大值15%的数据归零，另一列对应数据也为0
     * @param list
     * @return
     */
    public static List<PemsTestEntity> toZero(List<PemsTestEntity> list){
        double fuelMax = list.stream().map(PemsTestEntity::getFuel_CB_1).max(Comparator.naturalOrder()).orElse(null);
        list.stream().map(s ->{
            if(s.getFuel_CB_1() < 0.15 * fuelMax){
                s.setFuel_CB_1(0.0);
            }
            return s;
        }).collect(Collectors.toList());
        if (!(list.get(0).getOBD_EngineFuelRate() == null)){
            double obdMax =list.stream()
                    .map(PemsTestEntity::getOBD_EngineFuelRate)
                    .max(Comparator.naturalOrder()).orElse(null);
            list.stream().map(s ->{
                if(s.getOBD_EngineFuelRate() < 0.15 * obdMax){
                    s.setOBD_EngineFuelRate(0.0);
                    s.setFuel_CB_1(0.0);
                }
                if(s.getFuel_CB_1() == 0.0){
                    s.setOBD_EngineFuelRate(0.0);
                }
                return s;
            }).collect(Collectors.toList());
        }
        if (!(list.get(0).getOBDFuelRate() == null)){
            double obdMax =list.stream()
                    .map(PemsTestEntity::getOBDFuelRate)
                    .max(Comparator.naturalOrder()).orElse(null);
            list.stream().map(s ->{
                if(s.getOBDFuelRate() < 0.15 * obdMax){
                    s.setOBDFuelRate(0.0);
                    s.setFuel_CB_1(0.0);
                }
                if(s.getFuel_CB_1() == 0.0){
                    s.setOBDFuelRate(0.0);
                }
                return s;
            }).collect(Collectors.toList());
        }
        return list;

    }

    /**
     *计算一致性
     * @param list
     * @return
     */
    public static Double getR2(List<PemsTestEntity> list){
        List fcb1_list = list.stream().map(PemsTestEntity::getFuel_CB_1).collect(Collectors.toList());
        List obd_efr_list = null;
        if (!(list.get(0).getOBD_EngineFuelRate() == null)){
            obd_efr_list = list.stream().map(PemsTestEntity::getOBD_EngineFuelRate).collect(Collectors.toList());

        }else if (!(list.get(0).getOBDFuelRate() == null)){
            obd_efr_list = list.stream().map(PemsTestEntity::getOBDFuelRate).collect(Collectors.toList());
        }
        Double r2 = DataProcess.correlation(obd_efr_list,fcb1_list);

        return r2;
    }

    /**
     * @描述:集合负值修订为0
     * @param list
     * @return
     */
    public static List<Double> negativeToZero(List<Double> list){

        for (int i = 0;i < list.size();i++){
            list.set(i,Math.max(0,list.get(i)));
        }
        return list;
    }

    /**
     *@描述: 相关系数 <br/>
     *@方法名: correlation <br/>
     *@param x <br/>
     *@param y <br/>
     *@return  * @返回类型 double 返回值[-1,1]{[-1,0]负相关；[0,1]正相关}， 取绝对值，越大表示相关性越强,
     *          .8~1： 非常强 ，.6~.8： 强相关 ，.4~.6： 中度相关，.2~.4： 弱相关，.0~.： 弱相关或者无关 <br/>
     *@since <br/>
     *@throws <br/>
     */
    public static double correlation(List<Double> x, List<Double> y) {
        if (x.size() != y.size()) {
            throw new java.lang.NumberFormatException();
        }
        double xSum = 0;
        double ySum = 0;
        double xP2Sum = 0;
        double yP2Sum = 0;
        double xySum = 0;
        int len = x.size();
        for (int i = 0; i < y.size(); i++) {

            xSum = Mutil.add(xSum, x.get(i));
            ySum = Mutil.add(ySum, y.get(i));
            xP2Sum = Mutil.add(xP2Sum, Math.pow(x.get(i), 2));
            yP2Sum = Mutil.add(yP2Sum, Math.pow(y.get(i), 2));
            xySum = Mutil.add(xySum, Mutil.multiply(x.get(i), y.get(i)));

        }
        double Rxy = Mutil.subtract(Mutil.multiply(len, xySum), Mutil.multiply(xSum, ySum)) / (Math.sqrt((Mutil.multiply(len, xP2Sum) - Math.pow(xSum, 2)) * (Mutil.multiply(len, yP2Sum) - Math.pow(ySum, 2))));
        //return Mutil.round(Rxy, 4);
        return Mutil.round(Math.pow(Rxy,2), 4);

    }

    /**
     * Map转实体类
     * @param map 需要初始化的数据，key字段必须与实体类的成员名字一样，否则赋值为空
     * @param entity  需要转化成的实体类
     * @return
     */
    public static <T> T mapToEntity(Map<String, String> map, Class<T> entity){
        T t = null;
        try {
            t = entity.newInstance();
            for(Field field : entity.getDeclaredFields()) {
                if (map.containsKey(field.getName())) {
                    boolean flag = field.isAccessible();
                    field.setAccessible(true);
                    String object = map.get(field.getName());
                    if (object!= null) {
                        //数据类型转换
                        if (field.getType().isAssignableFrom(String.class)){
                            field.set(t, object);
                        }else if (field.getType().isAssignableFrom(Double.class)){
                            Double dobject;
                            dobject = Double.valueOf(object);
                            field.set(t,dobject);
                        }else if(field.getType().isAssignableFrom(Integer.class)){
                            Integer iobject;
                            iobject = Integer.valueOf(object).intValue();
                            field.set(t,iobject);
                        }else if(field.getType().isAssignableFrom(Boolean.class)){
                            Boolean bobject;
                            bobject = Boolean.valueOf(object);
                            field.set(t,bobject);
                        }else if(field.getType().isAssignableFrom(LocalDate.class)){
                            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDate dobject;
                            dobject = LocalDate.parse(object, dateFormatter);

                            field.set(t,dobject);
                        }
                    }
                    field.setAccessible(flag);
                }
            }
            return t;
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return t;
    }

    /**
     * @描述 有效窗口数据集合，size为有效窗口数。OBD_Power1保存平均值，其余保存求和值
     * @param datalist
     * @param circulationpower
     * @return
     */
    public static List<PemsTestEntity> getWindData(List<PemsTestEntity> datalist,double circulationpower){
        List<PemsTestEntity> datawind_list = new ArrayList<>();
        int num =datalist.size();
        for(int i = 0; i < num; i++){
            PemsTestEntity pte_power = new PemsTestEntity();
            boolean flag = false;

            Double obd_power = 0.0;
            double co = 0.0;
            double co2 = 0.0;
            double nox = 0.0;
            double pn = 0.0;
            for (int j = i; j < num; j++){
                obd_power = obd_power + datalist.get(j).getOBD_Power();
                co = co + datalist.get(j).getCO_MASS_1();
                co2 = co2 + datalist.get(j).getCO2_MASS_1();
                nox = nox + datalist.get(j).getNOx_MASS_1();
                pn = pn + datalist.get(j).getPN_Inst_1();

                Double obd_power1 = obd_power/(j-i);

                if(obd_power >= circulationpower){

                    try {
                        pte_power.setOBD_Power(obd_power);
                        pte_power.setCO_MASS_1(co);
                        pte_power.setCO2_MASS_1(co2);
                        pte_power.setNOx_MASS_1(nox);
                        pte_power.setPN_Inst_1(pn);

                        pte_power.setOBD_Power1(obd_power1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                if(j == num - 1 & obd_power < circulationpower){
                    flag = true;
                }
            }
            if(flag){
                break;
            }
            datawind_list.add(pte_power);
        }
        return datawind_list;
    }




    /**
     *@描述: 相关系数 <br/>
     *@方法名: correlation <br/>
     *@param x <br/>
     *@param y <br/>
     *@return  * @返回类型 double 返回值[-1,1]{[-1,0]负相关；[0,1]正相关}， 取绝对值，越大表示相关性越强,
     *          .8~1： 非常强 ，.6~.8： 强相关 ，.4~.6： 中度相关，.2~.4： 弱相关，.0~.： 弱相关或者无关 <br/>
     * @创建人 micheal <br/>
     *@创建时间 2019年1月3日下午8:51:27 <br/>
     *@修改人 micheal <br/>
     *@修改时间 2019年1月3日下午8:51:27 <br/>
     *@修改备注 <br/>
     *@since <br/>
     *@throws <br/>
     */
    public static double correlation(double[] x, double[] y) {
        if (x.length != y.length) {
            throw new java.lang.NumberFormatException();
        }
        double xSum = 0;
        double ySum = 0;
        double xP2Sum = 0;
        double yP2Sum = 0;
        double xySum = 0;
        int len = x.length;
        for (int i = 0; i < y.length; i++) {

            xSum = Mutil.add(xSum, x[i]);
            ySum = Mutil.add(ySum, y[i]);
            xP2Sum = Mutil.add(xP2Sum, Math.pow(x[i], 2));
            yP2Sum = Mutil.add(yP2Sum, Math.pow(y[i], 2));
            xySum = Mutil.add(xySum, Mutil.multiply(x[i], y[i]));

        }
        double Rxy = Mutil.subtract(Mutil.multiply(len, xySum), Mutil.multiply(xSum, ySum)) / (Math.sqrt((Mutil.multiply(len, xP2Sum) - Math.pow(xSum, 2)) * (Mutil.multiply(len, yP2Sum) - Math.pow(ySum, 2))));
        //return Mutil.round(Rxy, 4);
        return Rxy;
    }



}
