package com.pemstest.utils;

/**
 * 结果对象
 *
 * @author inspur
 */
public class Result implements java.io.Serializable {

    public static Result error() {
        return error("未知错误，请联系管理员");
    }

    public static Result error(String msg) {
        Result result = new Result();
        result.success = false;
        result.msg = msg;
        return result;
    }

    public static Result ok() {
        return ok("");
    }

    public static Result ok(String msg) {
        Result result = new Result();
        result.success = true;
        result.msg = msg;
        return result;
    }


    private boolean success = false;

    private String msg = "";

    private Object obj = null;

    /**
     * 是否成功
     *
     * @return
     */
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * 消息内容
     *
     * @return
     */
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 消息包含的对象信息
     *
     * @return
     */
    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

}
