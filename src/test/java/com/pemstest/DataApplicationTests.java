package com.pemstest;

import com.pemstest.domain.pems.PemsReportEntity;
import com.pemstest.service.pems.EmissionDataService;
import com.pemstest.utils.DataProcess;
import com.pemstest.domain.pems.PemsTestEntity;
import com.pemstest.utils.Mutil;
import com.pemstest.utils.ReadCSV;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataApplicationTests {

    @Autowired
    private EmissionDataService emissionDataService;

    @Test
    public void test() throws Exception {
        String inpath="C:\\Users\\KYLE\\Desktop\\PEMSTest_20200922_085729_524.csv";
        ReadCSV csvRead = new ReadCSV();
        List<Map<String,String>> retList = csvRead.getcsvTableList(inpath,3);
        List<PemsTestEntity> datalist = new ArrayList<>();

        PemsTestEntity pemsTestEntity;
        for (Map<String,String> m:retList){
            pemsTestEntity = DataProcess.mapToEntity(m,PemsTestEntity.class);
            datalist.add(pemsTestEntity);
        }
        DataProcess dp = new DataProcess();
        List<PemsTestEntity>  realdatalist = dp.getRealData(datalist);
        System.out.println(dp.result.getMsg());

        List<PemsTestEntity> test = new ArrayList<>(realdatalist.subList(1,2));
        List<PemsTestEntity> testlist = new ArrayList<>(realdatalist.subList(3,10));
        System.out.println("testlist长度：" + testlist.size());
        testlist.add(test.get(0));
        realdatalist.add(test.get(0));
        System.out.println("testlist长度：" + testlist.size());
        System.out.println("realdatalist长度：" + realdatalist.size());

        //额定功率
        double ratedpower =118;
        //循环功率
        double circulationpower =10.01;
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("circulationPower", "118");
        request.addParameter("ratedPower", "10.01");
        Object pemsReportEntity = new PemsReportEntity();
        pemsReportEntity = emissionDataService.proceeData(realdatalist,request);

        System.out.println(pemsReportEntity);
    }

    @Test
    public void sublist(){
        List list = new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        List sublist = new ArrayList<>(list.subList(1,3));
        list.add("5");
        sublist.add("5");
        System.out.println(list);
        System.out.println(sublist);
    }

    @Test
    public void contextLoads() {

        String inpath="C:\\Users\\KYLE\\Desktop\\PEMSTest_20200922_085729_524.csv";
        //String inpath="C:\\Users\\KYLE\\Desktop\\PEMSTest.csv";

        ReadCSV csvRead = new ReadCSV();
        List<Map<String,String>> retList = csvRead.getcsvTableList(inpath,3);
        List<PemsTestEntity> datalist = new ArrayList<>();

        //额定功率
        double ratedpower =404;
        //循环功率
        double circulationpower =39.3;

        PemsTestEntity pemsTestEntity;
        for (Map<String,String> m:retList){
            pemsTestEntity = DataProcess.mapToEntity(m,PemsTestEntity.class);
            datalist.add(pemsTestEntity);
        }
        System.out.println("处理前数据条数：" + datalist.size());
        List<PemsTestEntity> realdatalist = null;
        int realdata_num = 0;
        try {
            DataProcess dp = new DataProcess();
            realdatalist = dp.getRealData(datalist);
            realdata_num = realdatalist.size();
            System.out.println("处理后数据条数：" + realdata_num);
        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("实验时间(h)是： " + (double) realdata_num /3600);

        assert realdatalist != null;
        DoubleSummaryStatistics gps_vs = realdatalist.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getGPS_VehicleSpeed));
        double mileage_sum = gps_vs.getSum()/3600;
        System.out.println("行驶里程是： " + mileage_sum);



        DoubleSummaryStatistics ws_at = realdatalist.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getWS_AmbientTemp));
        System.out.println("大气温度最大值是： " + ws_at.getMax());
        System.out.println("大气温度最小值是： " + ws_at.getMin());
        System.out.println("大气温度平均值是： " + ws_at.getAverage());

        DoubleSummaryStatistics ws_arh = realdatalist.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getWS_AmbientRelativeHumidity));
        System.out.println("大气湿度最大值是： " + ws_arh.getMax());
        System.out.println("大气湿度最小值是： " + ws_arh.getMin());
        System.out.println("大气湿度平均值是： " + ws_arh.getAverage());

        DoubleSummaryStatistics ws_ap = realdatalist.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getWS_AmbientPressure));
        System.out.println("大气压力最大值是： " + ws_ap.getMax());
        System.out.println("大气压力最小值是： " + ws_ap.getMin());
        System.out.println("大气压力平均值是： " + ws_ap.getAverage());

        DoubleSummaryStatistics gps_a = realdatalist.stream().collect(Collectors.summarizingDouble(PemsTestEntity::getGPS_Altitude));
        System.out.println("海拔最大值是： " + gps_a.getMax());
        System.out.println("海拔最小值是： " + gps_a.getMin());
        System.out.println("海拔平均值是： " + gps_a.getAverage());
        double gps_astart = realdatalist.get(0).getGPS_Altitude();
        double gps_aend = realdatalist.get(realdatalist.size() -1).getGPS_Altitude();
        System.out.println("海拔起始值是： " + gps_astart + "。海拔终点值是： " + gps_aend + "。海拔落差是： " + Math.abs(gps_aend - gps_astart) );

        List<Double> old_gps_a = realdatalist.stream().map(PemsTestEntity::getGPS_Altitude).collect(Collectors.toList());
        List<Double> new_gps_a = new ArrayList<>();
        new_gps_a.add(0.0);
        for (int i = 1; i < realdata_num; i++){
            new_gps_a.add(Math.max(0,(old_gps_a.get(i)-old_gps_a.get(i-1))));
        }
        double sum_gps_a = new_gps_a.stream().mapToDouble(s -> s).sum();
        System.out.println("累计海拔增量:" + 100 * sum_gps_a/mileage_sum);

        List<Double> old_gps_vs =realdatalist.stream().map(PemsTestEntity::getGPS_VehicleSpeed).collect(Collectors.toList());
        List<Double> new_gps_vs = new ArrayList<>();
        new_gps_vs.add(0.0);
        for (int i = 1;i < realdata_num -1;i++){
            new_gps_vs.add((old_gps_vs.get(i+1)-old_gps_vs.get(i-1))/7.2);
        }
        new_gps_vs.add((old_gps_vs.get(realdata_num -1)-old_gps_vs.get(realdata_num-2))/3.6);

        double idle_num = old_gps_vs.stream().filter(s ->s < 1).count();
        double upspeed_num = new_gps_vs.stream().filter(s -> s >= 0.1).count();
        double downspeed_num = new_gps_vs.stream().filter(s -> s <= -0.1).count();

        System.out.println("加速个数" + upspeed_num + "和" + realdata_num);
        System.out.println("加速比例是： " + upspeed_num/realdata_num);
        System.out.println("减速比例是： " + downspeed_num/realdata_num);
        System.out.println("匀速比例是： " + (realdata_num - upspeed_num - downspeed_num -idle_num)/realdata_num);
        System.out.println("怠速比例是： " + idle_num/realdata_num);

        int suburb_start = 0;
        int suburb_tar = 0;
        int highe_tar = 0;
        for (int i = 1;i < realdata_num -1;i++){
            if (old_gps_vs.get(i) >= 55){
                suburb_tar = i;
                break;
            }
        }
        for (int i = suburb_tar;i >= 0;i--){
            if (old_gps_vs.get(i) < 1){
                suburb_start =i;
                break;
            }
        }
        int high_start = 0;
        for (int i = suburb_tar;i < realdata_num;i++){
            if (old_gps_vs.get(i) > 75){
                highe_tar =i;
                break;
            }
        }
        for  (int i = highe_tar;i > suburb_tar;i--){
            if (old_gps_vs.get(i) < 1){
                high_start =i;
                break;
            }
        }

        System.out.println("市郊开始车速" + datalist.get(suburb_start).getGPS_VehicleSpeed()  + "高速开始车速" + datalist.get(high_start).getGPS_VehicleSpeed() );

        System.out.println("市区时间占比：" + (double) (suburb_start) / (double) realdata_num);
        System.out.println("市郊时间占比：" + (double) (high_start - suburb_start) / (double) realdata_num);
        System.out.println("高速时间占比：" + (double) (realdata_num - high_start + 1) / (double) realdata_num);
        System.out.println("郊区起始点:" + suburb_start + "。高速路起点:" + high_start + "。总点数:" + realdata_num);

        double city_speed = old_gps_vs.subList(0, suburb_start -1).stream().collect(Collectors.averagingDouble(s -> s));
        double suburb_speed = old_gps_vs.subList(suburb_start, high_start -1).stream().collect(Collectors.averagingDouble(s -> s));
        double high_speed = old_gps_vs.subList(high_start, realdata_num).stream().collect(Collectors.averagingDouble(s -> s));

        System.out.println("市区平均速度：" + city_speed);
        System.out.println("市郊平均速度：" + suburb_speed);
        System.out.println("高速平均速度：" + high_speed);


        List<Double> obd_efr_list = realdatalist.stream().map(PemsTestEntity::getOBD_EngineFuelRate).collect(Collectors.toList());
        List<Double> fcb1_list = realdatalist.stream().map(PemsTestEntity::getFuel_CB_1).collect(Collectors.toList());
        //double tt = DataProcess.correlation(obd_efr_list,fcb1_list);
        System.out.println("一致性相关系数" + DataProcess.correlation(obd_efr_list,fcb1_list));

        //扭矩计算
        List<Double> powerlist = realdatalist.stream()
                .map(s -> (s.getOBD_ActualEnginePercentTorque() - s.getOBD_NominalFrictionPercentTorque())* s.getOBD_EngineReferenceTorque()/100).collect(Collectors.toList());
        //总功
        Double g_sum = realdatalist.stream().map(s -> Math.max(0, (s.getOBD_ActualEnginePercentTorque() - s.getOBD_NominalFrictionPercentTorque())) * s.getOBD_EngineReferenceTorque() * s.getOBD_EngineSpeed() / 100 / 9550 / 3600).mapToDouble(t -> t).sum();
        System.out.println("实验总功是：" + g_sum);


        //计算功率
        realdatalist
                .stream()
                .map(s ->{
                    Double ll = Math.max(0,(s.getOBD_ActualEnginePercentTorque() - s.getOBD_NominalFrictionPercentTorque()))* s.getOBD_EngineReferenceTorque() *s.getOBD_EngineSpeed()/100/9550/3600;
                    s.setOBD_Power(ll);
                    return s;
                }).collect(Collectors.toList());


        //生成新的集合，OBD_Power1保存平均值，其余保存求和值
        List<PemsTestEntity> list_win = new ArrayList<>();
        for(int i = 0; i < realdata_num; i++){
            PemsTestEntity pte_power = new PemsTestEntity();
            boolean flag = false;

            Double obd_power = 0.0;
            double co = 0.0;
            double co2 = 0.0;
            double nox = 0.0;
            double pn = 0.0;
            for (int j = i; j < realdata_num; j++){
                obd_power = obd_power + realdatalist.get(j).getOBD_Power();
                co = co + realdatalist.get(j).getCO_MASS_1();
                co2 = co2 + realdatalist.get(j).getCO2_MASS_1();
                nox = nox + realdatalist.get(j).getNOx_MASS_1();
                //pn = pn + realdatalist.get(j).getPN_Inst_1();

                Double obd_power1 = obd_power/(j-i);

                if(obd_power >= circulationpower){

                    try {
                        pte_power.setOBD_Power(obd_power);
                        pte_power.setCO_MASS_1(co);
                        pte_power.setCO2_MASS_1(co2);
                        pte_power.setNOx_MASS_1(nox);
                        pte_power.setPN_Inst_1(pn);

                        pte_power.setOBD_Power1(obd_power1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                if(j == realdata_num - 1 & obd_power < circulationpower){
                    flag = true;
                }
            }
            if(flag){
                break;
            }
            list_win.add(pte_power);

        }
        System.out.println("窗口数" + list_win.size());


        Integer over_num = 0;
        //有效窗口数
        List<PemsTestEntity> realwind_list = new ArrayList<>();

        for (int i = 20;i > 0;i--){
            int finalI = i;
            realwind_list = list_win.stream()
                    .filter(s -> s.getOBD_Power1()*3600/ratedpower*100 > finalI)
                    .collect(Collectors.toList());
            if (realwind_list.size() > list_win.size()*0.5){
                over_num = i;
                break;
            }
        }
        int realwind_num = realwind_list.size();
        System.out.println("有效窗口的功率阈值" + over_num);
        System.out.println("有效窗口数" + realwind_num);

        //排放处理，将负值修正为零而不是去掉负值。
        DoubleSummaryStatistics dnox =realdatalist.stream().map(s -> Math.max(0,s.getGA_NOxConc()))
                .collect(Collectors.summarizingDouble(s -> s));

        System.out.println("NOX浓度最大值是： " + dnox.getMax());
        System.out.println("NOX浓度最小值是： " + dnox.getMin());
        System.out.println("NOX浓度平均值是： " + dnox.getAverage());
        long morefh = realdatalist.stream().filter(pemsTestEntity1 -> pemsTestEntity1.getGA_NOxConc() < 500).count();
        System.out.println("浓度小于500的比例" + (double) morefh /realdata_num);
        //从小到大排序后最接近95%的浓度值。
        double tar_nox = realdatalist.stream().map(PemsTestEntity::getGA_NOxConc).sorted().skip(Math.round(0.95*realdata_num)-1).limit(1).collect(Collectors.toList()).get(0);
        System.out.println("排序接近95%的浓度" + tar_nox);

        //CO排放总量,单位g变mg
        double co_sum = realdatalist.stream().map(s -> Math.max(0, s.getCO_MASS_1())).mapToDouble(s -> s).sum() *1000;
        System.out.println("CO平均排放" +  co_sum/mileage_sum);
        System.out.println("CO平均比排放" +  co_sum/g_sum);

        //CO合格窗口数
        float co_num =realwind_list.stream().filter(s -> s.getCO_MASS_1() < s.getOBD_Power() * 6).count();
        double x = co_num/realwind_list.size();
        System.out.println("CO窗口通过率" + x);


        //NOX排放总量,单位g变mg
        double nox_sum = realdatalist.stream().map(s -> Math.max(0, s.getNOx_MASS_1())).mapToDouble(s -> s).sum() *1000;
        System.out.println("NOX平均排放" +  nox_sum/mileage_sum);
        System.out.println("NOX平均比排放" +  nox_sum/g_sum);

        //NOX合格窗口数
        float nox_num =realwind_list.stream().filter(s -> s.getNOx_MASS_1() < s.getOBD_Power() * 0.69).count();
        System.out.println("NOX窗口通过率" + nox_num/realwind_num);


        //PN排放总量
        //double pn_sum = realdatalist.stream().map(s -> Math.max(0, s.getPN_Inst_1())).mapToDouble(s -> s).sum();
        //System.out.println("pN平均排放" +  pn_sum/mileage_sum);
        //System.out.println("PN平均比排放" +  pn_sum/g_sum);

        //PN合格窗口数
        //float pn_num =realwind_list.stream().filter(s -> s.getPN_Inst_1() < s.getOBD_Power() * 1.2E12).count();
        //System.out.println("PN窗口通过率" + nox_num/realwind_num);

        //CO2排放总量,单位g变mg
        double co2_sum = realdatalist.stream().map(s -> Math.max(0, s.getCO2_MASS_1())).mapToDouble(s -> s).sum() *1000;
        System.out.println("CO2平均排放" +  co2_sum/mileage_sum);
        System.out.println("CO2平均比排放" +  co2_sum/g_sum);

    }

}
